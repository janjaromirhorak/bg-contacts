<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

use App\Entity\Contact;

class HomepageController extends AbstractController
{
    /**
     * Controller for the home page. Fetches all contacts and displays
     * them using a template.
     *
     * @Route("/", name="homepage")
     */
    public function homepage()
    {
        // retrieve all contacts
        $repository = $this->getDoctrine()->getRepository(Contact::class);
        $contacts = $repository->findAll();

        // initialize a session (used to retrieve flash messages)
        $session = new Session();

        return $this->render('pages/contactList.html.twig', [
            // pass all contacts and flash messages to the template
            'contacts' => $contacts,
            'allMessages' => $session->getFlashBag()->all(),
        ]);
    }
}
