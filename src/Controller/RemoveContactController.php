<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

use App\Entity\Contact;

class RemoveContactController extends AbstractController
{
    /**
     * Removes a contact specified by ID from the database.
     * This controller does not render anything, it handles
     * the contact removal, sets up a flash message and redirects to homepage.
     *
     * @param  number $id ID of the contact
     *
     * @Route("/remove/{id}", name="remove")
     */
    public function remove($id)
    {
        $session = new Session();

        // retrieve the contact from DB
        $entityManager = $this->getDoctrine()->getManager();
        $contact = $entityManager->getRepository(Contact::class)->find($id);

        // handle the case when the contact does not exist
        if (!$contact) {
            // set up a flash message
            $session->getFlashBag()->add(
                'error',
                'Kontakt nebyl nalezen, nemůže být tedy odstraněn.'
            );

            // redirect to homepage
            return $this->redirectToRoute('homepage');
        }

        // remove the contact from the database
        $entityManager->remove($contact);
        $entityManager->flush();

        // set up a flash message
        $session->getFlashBag()->add(
            'success',
            'Kontakt odstraněn.'
        );

        // redirect to homepage
        return $this->redirectToRoute('homepage');
    }
}
