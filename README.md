# Adresář kontaktů

Jednoduchá webová aplikace vytvořená dle [zadání](zadani.pdf) pro pracovní pohovor. Využívá frameworku Symfony.

## Požadavky

-   PHP verze 7.1.3 nebo vyšší
-   [composer](https://getcomposer.org/) a další [standardní požadavky frameworku Symfony](https://symfony.com/doc/current/reference/requirements.html)

## Instalace

```sh
composer install
```

Po instalaci aplikace je nutno nakonfigurovat připojení k databázi v souboru `.env`,
následně pak provést [migraci databáze](https://symfony.com/doc/current/doctrine.html#migrations-creating-the-database-tables-schema):
```sh
# V případě, že databáze nastavená v .env ještě nebyla vytvořena, lze ji vytvořit pomocí Doctrine:
php bin/console doctrine:database:create
```

```sh
# Vygeneruje plán migrace
php bin/console make:migration
# Provede plán migrace
php bin/console doctrine:migrations:migrate
```

## Vývojový server

Vývojový server naslouchající na loopback IP `127.0.0.1` na portu `8000` lze spustit následujícím příkazem:

```sh
php bin/console server:run
```
