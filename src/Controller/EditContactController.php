<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

use App\Entity\Contact;
use App\Form\ContactType;

class EditContactController extends AbstractController {
    /**
     * Display and process a form that allows users to edit a contact
     * from the address book, identified by an identifier.
     * (The format of the identifier is specified in App\Entity\Contact
     * repository of this entity contains a function findByIdentifier that
     * allows to retrieve the correct contact from the database.)
     *
     * @param string $identifier
     * @param Request $request The page request. Used to handle form submission.
     *
     * @Route("/{identifier}", name="edit")
     */
    public function edit($identifier, Request $request)
    {
        $session = new Session();

        $entityManager = $this->getDoctrine()->getManager();

        // find the contact by identifier
        $contact = $entityManager->getRepository(Contact::class)->findByIdentifier($identifier);

        // handle the case when the contact does not exist
        if (!$contact) {
            // set up a flash message
            $session->getFlashBag()->add(
                'error',
                'Kontakt nebyl nalezen.'
            );

            // redirect to homepage
            return $this->redirectToRoute('homepage');
        }

        // the contact exists -> create a form
        $form = $this->createForm(ContactType::class, $contact);

        // handle form submission
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            // apply changes to the database
            $entityManager->persist($contact);
            $entityManager->flush();

            // set up a flash message
            $session->getFlashBag()->add(
                'success',
                'Kontakt aktualizován'
            );

            // redirect to homepage
            return $this->redirectToRoute('homepage');
        }

        // if the identifier differs from the current identifier,
        // redirect to the current one (so there is always exactly one correct address for each contact)
        if($identifier !== $contact->getIdentifier()) {
            return $this->redirectToRoute('edit', [
                'identifier' => $contact->getIdentifier()
            ]);
        }

        // the identifier is up to date and the form has not been
        // submitted yet -> display the form
        return $this->render('pages/editContact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
