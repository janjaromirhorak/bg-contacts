<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Regex("/^\+?[0-9 ]+$/")
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\Email(
     *     message = "{{ value }} není validní e-mailová adresa.",
     *     checkMX = false
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $note;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * get a phone number formatted with spaces for better readability
     *
     * @return string formatted phone number
     */
    public function getFormattedPhone(): ?string
    {
        $phoneString = "";
        $numberLength = strlen($this->phone);

        // iterate over the string and add spaces to the correct places
        for($i = 0 ; $i < $numberLength ; $i++) {
            // add a space before every three digits from the end
            if($i !== 0 && ($numberLength - $i) % 3 === 0 &&
                // except the space after '+' at the beginning of the number
                !($i === 1 && $this->phone[0] === '+')) {
                $phoneString .= ' ';
            }
            $phoneString .= $this->phone[$i];
        }

        return $phoneString;
    }

    public function setPhone(?string $phone): self
    {
        // strip spaces from the phone number
        $this->phone = str_replace(' ', '', $phone);

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    // custom getters

    /**
     * get a SEO optimized identifier in the following format: `id-firstName-lastName`
     * @return string url encoded and SEO optimized identifier
     */
    public function getIdentifier(): string
    {
        return urlencode($this->id . "-" . $this->firstName . "-" . $this->lastName);
    }
}
