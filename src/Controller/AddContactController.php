<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

use App\Entity\Contact;
use App\Form\ContactType;

class AddContactController extends AbstractController
{
    /**
    * Display and process a form that allows users to add a new contact
    * to the address book.
    *
    * @Route("/add", name="add")
    */
    public function add(Request $request)
    {
        // build the form
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);

        // handle the submit
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            // write the changes to the database
            $entityManager->persist($contact);
            $entityManager->flush();

            // set up a flash message
            $session = new Session();
            $session->getFlashBag()->add(
                'success',
                'Kontakt přidán'
            );

            // redirect to the homepage
            return $this->redirectToRoute('homepage');
        }

        // the form has not been submitted -> display the form
        return $this->render('pages/addContact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
